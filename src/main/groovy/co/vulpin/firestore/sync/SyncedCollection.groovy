package co.vulpin.firestore.sync

import com.google.cloud.firestore.CollectionReference

abstract class SyncedCollection<E extends Entity> {

    protected Map<String, E> entities = [:]

    protected CollectionReference reference
    protected Class<E> entityClass

    SyncedCollection(CollectionReference reference, Class<E> entityClass) {
        this.reference = reference
        this.entityClass = entityClass
    }

    abstract E get(String id)

    CollectionReference getReference() {
        return reference
    }

}

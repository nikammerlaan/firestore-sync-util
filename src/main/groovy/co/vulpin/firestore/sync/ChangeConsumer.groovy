package co.vulpin.firestore.sync

interface ChangeConsumer<E> {

    void consume(E oldValue, E newValue)

}

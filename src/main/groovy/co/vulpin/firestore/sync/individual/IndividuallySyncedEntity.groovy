package co.vulpin.firestore.sync.individual

import co.vulpin.firestore.sync.SyncedEntity
import com.google.cloud.firestore.DocumentReference
import com.google.cloud.firestore.ListenerRegistration
import groovy.util.logging.Slf4j

import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@Slf4j
class IndividuallySyncedEntity extends SyncedEntity {

    protected CountDownLatch readyLatch = new CountDownLatch(1)
    protected ListenerRegistration listener

    IndividuallySyncedEntity(DocumentReference reference) {
        super(reference)

        listener = reference.addSnapshotListener({ snapshot, e ->
            try {
                handleSnapshot(snapshot)
            } finally {
                readyLatch.countDown()
            }
        })
    }

    boolean awaitReady(long amount = 1, TimeUnit unit = TimeUnit.SECONDS) {
        return readyLatch.await(amount, unit)
    }

}

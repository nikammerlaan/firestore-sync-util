package co.vulpin.firestore.sync.individual

import co.vulpin.firestore.sync.SyncedCollection
import groovy.transform.InheritConstructors

@InheritConstructors
class IndividuallySyncedCollection<E extends IndividuallySyncedEntity> extends SyncedCollection<E> {

    @Override
    synchronized E get(String id) {
        def ref = reference.document(id)
        return entities.computeIfAbsent(id, {
            entityClass.newInstance(ref)
        })
    }

}

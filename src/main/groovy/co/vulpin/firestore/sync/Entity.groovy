package co.vulpin.firestore.sync

import com.google.cloud.firestore.CollectionReference
import com.google.cloud.firestore.DocumentReference

class Entity {

    final DocumentReference reference

    Entity(DocumentReference reference) {
        this.reference = reference
    }

    Entity(CollectionReference collectionReference) {
        this(collectionReference.document())
    }

    Entity() {
        this.reference = null
    }

    String getId() {
        return reference.id
    }

    @Override
    boolean equals(Object obj) {
        if(obj instanceof Entity) {
            return this.reference == obj.reference
        } else {
            return false
        }
    }
    
}

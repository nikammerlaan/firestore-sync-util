package co.vulpin.firestore.sync

import com.google.cloud.firestore.DocumentSnapshot
import com.google.cloud.firestore.SetOptions
import groovy.transform.InheritConstructors
import groovy.util.logging.Slf4j

import java.util.concurrent.ConcurrentHashMap

@InheritConstructors
@Slf4j
class SyncedEntity extends Entity {

    protected Map<String, Set<ChangeConsumer>> consumers = new ConcurrentHashMap<>()

    void onChange(String field, ChangeConsumer consumer) {
        consumers.computeIfAbsent(field, {[]}).add(consumer)
    }

    void handleSnapshot(DocumentSnapshot snapshot) {
        snapshot.data?.entrySet()?.sort { it.key }?.each { entry ->
            def field = entry.key
            def newValue = entry.value
            def oldValue = getProperty(field)
            if (newValue != oldValue)
                handleChange(field, oldValue, newValue)
        }
    }

    void handleChange(String field, Object oldValue, Object newValue) {
        this.@"$field" = newValue // directly sets the property to avoid using the overloaded setProperty method

        consumers.get(field)?.each {
            try {
                it.consume(oldValue, newValue)
            } catch (Exception e) {
                e.printStackTrace()
                log.error("Error while processing change consumer", e)
            }
        }
    }

    @Override
    void setProperty(String field, Object newValue) {
        def cur = this.@"$field"
        if(cur != newValue) {
            def vals = [:]
            vals[field] = newValue
            reference.set(vals, SetOptions.merge())
        }
    }

}

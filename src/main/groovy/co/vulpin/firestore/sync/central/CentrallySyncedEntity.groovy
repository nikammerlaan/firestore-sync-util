package co.vulpin.firestore.sync.central

import co.vulpin.firestore.sync.SyncedEntity
import groovy.transform.InheritConstructors

@InheritConstructors
class CentrallySyncedEntity extends SyncedEntity {}

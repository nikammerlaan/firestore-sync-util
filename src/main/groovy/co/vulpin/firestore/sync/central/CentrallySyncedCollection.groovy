package co.vulpin.firestore.sync.central

import co.vulpin.firestore.sync.SyncedCollection
import com.google.cloud.firestore.CollectionReference
import com.google.cloud.firestore.QuerySnapshot

import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

import static com.google.cloud.firestore.DocumentChange.Type.ADDED
import static com.google.cloud.firestore.DocumentChange.Type.MODIFIED
import static com.google.cloud.firestore.DocumentChange.Type.REMOVED

class CentrallySyncedCollection<E extends CentrallySyncedEntity> extends SyncedCollection<E> {

    protected CountDownLatch latch = new CountDownLatch(1)

    CentrallySyncedCollection(CollectionReference reference, Class<E> entityClass) {
        super(reference, entityClass)

        reference.addSnapshotListener({ snapshot, e ->
            try {
                handleSnapshot(snapshot)
            } finally {
                latch.countDown()
            }
        })
    }

    private void handleSnapshot(QuerySnapshot querySnap) {
        querySnap.documentChanges.each {
            def snap = it.document
            switch(it.type) {
                case ADDED:
                    def entity = entityClass.newInstance(snap.reference)
                    entities.put(snap.id, entity)
                    entity.handleSnapshot(snap)
                    break
                case MODIFIED:
                    def entity = entities.get(snap.id)
                    entity.handleSnapshot(snap)
                    break
                case REMOVED:
                    entities.remove(snap.id)
                    break
            }
        }
    }

    boolean awaitReady(long amount = 5, TimeUnit unit = TimeUnit.SECONDS) {
        return latch.await(amount, unit)
    }

    E get(String id) {
        awaitReady()
        return entities.get(id)
    }

    List<E> getDocuments() {
        def docs = entities.values().asList()

        awaitReady()

        return docs
    }

}
